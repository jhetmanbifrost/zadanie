package pl.ksz.investfunds.service;

import pl.ksz.investfunds.api.service.FundsGroupExtractorService;
import pl.ksz.investfunds.model.Fund;
import pl.ksz.investfunds.model.FundType;
import pl.ksz.investfunds.results.FundsGroup;

import java.util.List;
import java.util.stream.Collectors;

public class FundsGroupExtractorServiceImpl implements FundsGroupExtractorService {

    public FundsGroup extract(List<Fund> funds, FundType fundType) {
        FundsGroup fundsGroup = new FundsGroup(fundType);
        fundsGroup.setFunds(funds.stream().filter(fund -> fund.getType() == fundType).collect(Collectors.toList()));
        return fundsGroup;
    }

}

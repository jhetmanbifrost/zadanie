package pl.ksz.investfunds.service;

import pl.ksz.investfunds.api.RateProvider;
import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;
import pl.ksz.investfunds.api.service.InvestingFundShareResolverService;

public class InvestingFundShareResolverServiceImpl implements InvestingFundShareResolverService {

    public int getShare(RateProvider rateProvider, TotalInvestmentAmountProvider totalInvestmentAmountProvider) {
        return totalInvestmentAmountProvider.getTotalInvestmentAmount() * rateProvider.getRate() / 100;
    }
}

package pl.ksz.investfunds.service;

import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;
import pl.ksz.investfunds.api.service.InvestingFundShareResolverService;
import pl.ksz.investfunds.api.service.RatesConfigurationService;
import pl.ksz.investfunds.api.service.SharePerFundCalculationService;
import pl.ksz.investfunds.model.FundType;
import pl.ksz.investfunds.model.InvestingStyle;
import pl.ksz.investfunds.model.RatesConfiguration;
import pl.ksz.investfunds.model.RatesConfigurationId;

public class SharePerFundCalculationServiceImpl implements SharePerFundCalculationService {

    private RatesConfigurationService ratesConfigurationService;
    private InvestingFundShareResolverService investingFundShareResolverService;

    public SharePerFundCalculationServiceImpl(RatesConfigurationService ratesConfigurationService,
                                              InvestingFundShareResolverService investingFundShareResolverService) {
        this.ratesConfigurationService = ratesConfigurationService;
        this.investingFundShareResolverService = investingFundShareResolverService;
    }

    public int getSharePerFundsGroup(final TotalInvestmentAmountProvider totalInvestmentAmountProvider,
                                     final InvestingStyle investingStyle,
                                     final FundType fundType) {

        RatesConfigurationId ratesConfigurationId = new RatesConfigurationId(fundType, investingStyle);
        RatesConfiguration ratesConfiguration = ratesConfigurationService.getByConfigurationId(ratesConfigurationId);

        return investingFundShareResolverService.getShare(ratesConfiguration, totalInvestmentAmountProvider);
    }

    public void setRatesConfigurationService(RatesConfigurationService ratesConfigurationService) {
        this.ratesConfigurationService = ratesConfigurationService;
    }

    public void setInvestingFundShareResolverService(InvestingFundShareResolverService investingFundShareResolverService) {
        this.investingFundShareResolverService = investingFundShareResolverService;
    }
}
package pl.ksz.investfunds.service;

import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;
import pl.ksz.investfunds.api.service.PercentageCalculatorService;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class PercentageCalculatorServiceImpl implements PercentageCalculatorService {

    public static final int PRECISION = 4;
    public static final int SCALE = 2;

    private TotalInvestmentAmountProvider totalInvestmentAmountProvider;

    public String getComputedPercentage(int share) {
        BigDecimal percentage = new BigDecimal(share * 100.00f / totalInvestmentAmountProvider.getTotalInvestmentAmount(),
                new MathContext(PRECISION, RoundingMode.HALF_DOWN));
        return percentage
                .setScale(SCALE, RoundingMode.HALF_DOWN).stripTrailingZeros()
                .toPlainString() + '%';
    }

    @Override
    public void setTotalInvestmentAmountProvider(TotalInvestmentAmountProvider totalInvestmentAmountProvider) {
        this.totalInvestmentAmountProvider = totalInvestmentAmountProvider;
    }
}
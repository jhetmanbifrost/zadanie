package pl.ksz.investfunds.results;

import pl.ksz.investfunds.api.service.PercentageCalculatorService;

public class InvestmentDetails {

    public static class InvestmentDetailsBuilder {
        private PercentageCalculatorService percentageCalculator;
        private String fundName;
        private int share;

        public InvestmentDetailsBuilder (PercentageCalculatorService percentageCalculator) {
            this.percentageCalculator = percentageCalculator;
        }

        public InvestmentDetailsBuilder withFundName(String fundName) {
            this.fundName = fundName;
            return this;
        }

        public InvestmentDetailsBuilder withShare(int share) {
            this.share = share;
            return this;
        }

        public InvestmentDetails build() {
            String percentage = percentageCalculator.getComputedPercentage(share);
            return new InvestmentDetails(fundName, share, percentage);
        }
    }

    private String fundName;
    private int share;
    private String computedPercentage;

    private InvestmentDetails(String fundName, int share, String computedPercentage) {
        this.fundName = fundName;
        this.share = share;
        this.computedPercentage = computedPercentage;
    }

    public int getShare() {
        return share;
    }

    public String getFundName() {
        return fundName;
    }

    public String getComputedPercentage() {
        return computedPercentage;
    }
}

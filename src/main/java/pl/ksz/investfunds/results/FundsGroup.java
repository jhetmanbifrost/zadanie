package pl.ksz.investfunds.results;

import pl.ksz.investfunds.model.Fund;
import pl.ksz.investfunds.model.FundType;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FundsGroup {
    private FundType fundType;
    private List<Fund> funds;
    private int investmentPerGroup;

    public FundsGroup(FundType fundType) {
        this.fundType = fundType;
    }

    public int getSharePerFund() {
        if (isEmpty()) {
            return investmentPerGroup;
        }

        int numberOfFunds = funds.size();
        return investmentPerGroup / numberOfFunds;
    }

    public int getRemainder() {
        if (isEmpty()) {
            return 0;
        }

        int sharePerFund = getSharePerFund();
        int numberOfFunds = funds.size();
        return investmentPerGroup - sharePerFund * numberOfFunds;
    }

    public FundType getFundType() {
        return fundType;
    }

    public void setFundType(FundType fundType) {
        this.fundType = fundType;
    }

    public List<Fund> getFunds() {
        return Collections.unmodifiableList(funds);
    }

    public void setFunds(List<Fund> funds) {
        this.funds = funds;
    }

    public int getInvestmentPerGroup() {
        return investmentPerGroup;
    }

    public void setInvestment(int investment) {
        this.investmentPerGroup = investment;
    }

    public final boolean isEmpty() {
        return Objects.isNull(funds) || funds.isEmpty();
    }
}

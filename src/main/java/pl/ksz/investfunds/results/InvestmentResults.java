package pl.ksz.investfunds.results;

import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;

import java.io.Serializable;
import java.util.*;

public class InvestmentResults implements TotalInvestmentAmountProvider, Serializable {

    private List<InvestmentDetails> results = new ArrayList<>();

    private int notInvestedAmount;
    private final int totalInvestmentAmount;

    public InvestmentResults(int totalInvestmentAmount) {
        this.totalInvestmentAmount = totalInvestmentAmount;
    }

    public void addInvestmentDetails(InvestmentDetails investmentDetails) {
        results.add(investmentDetails);
    }

    public List<InvestmentDetails> getInvestmentDetails() {
        return Collections.unmodifiableList(results);
    }

    public void setNotInvestedAmount(int notInvestedAmount) {
        this.notInvestedAmount = notInvestedAmount;
    }

    public int getNotInvestedAmount() {
        return notInvestedAmount;
    }

    @Override
    public int getTotalInvestmentAmount() {
        return totalInvestmentAmount;
    }
}

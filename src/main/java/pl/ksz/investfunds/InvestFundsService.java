package pl.ksz.investfunds;

import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;
import pl.ksz.investfunds.api.service.FundService;
import pl.ksz.investfunds.api.service.FundsGroupExtractorService;
import pl.ksz.investfunds.api.service.PercentageCalculatorService;
import pl.ksz.investfunds.api.service.SharePerFundCalculationService;
import pl.ksz.investfunds.model.Fund;
import pl.ksz.investfunds.model.FundType;
import pl.ksz.investfunds.model.InvestingStyle;
import pl.ksz.investfunds.results.FundsGroup;
import pl.ksz.investfunds.results.InvestmentDetails;
import pl.ksz.investfunds.results.InvestmentResults;

import java.util.List;

public class InvestFundsService {

    private SharePerFundCalculationService sharePerFundCalculationService;
    private FundService fundService;
    private FundsGroupExtractorService fundsGroupExtractorService;
    private PercentageCalculatorService percentageCalculatorService;

    public InvestmentResults getComputedShares(final int totalInvestmentValue,
                                               final InvestingStyle investingStyle,
                                               final List<Long> chosenFunds) {

        final InvestmentResults investmentResults = new InvestmentResults(totalInvestmentValue);
        final List<Fund> funds = fundService.findByIdIsIn(chosenFunds);

        prepareResults(investmentResults, investingStyle, funds);

        return investmentResults;
    }

    private void prepareResults(final InvestmentResults investmentResults, final InvestingStyle investingStyle, final List<Fund> funds) {
        int usedAmount = 0;

        for (FundType fundType : FundType.values()) {

            FundsGroup fundsGroup = fundsGroupExtractorService.extract(funds, fundType);
            int sharePerFundsGroup = sharePerFundCalculationService.getSharePerFundsGroup(investmentResults, investingStyle, fundType);

            fundsGroup.setInvestment(sharePerFundsGroup);

            if (!fundsGroup.isEmpty()) {
                usedAmount += sharePerFundsGroup;
            }

            populateResults(investmentResults, fundsGroup);
        }

        investmentResults.setNotInvestedAmount(getTotalInvestmentAmount(investmentResults) - usedAmount);
    }

    private void populateResults(final InvestmentResults investmentResults, FundsGroup fundsGroup) {

        int remainder = fundsGroup.getRemainder();

        percentageCalculatorService.setTotalInvestmentAmountProvider(investmentResults);

        InvestmentDetails.InvestmentDetailsBuilder investmentDetailsBuilder =
                new InvestmentDetails.InvestmentDetailsBuilder(percentageCalculatorService);

        for (Fund fund : fundsGroup.getFunds()) {
            InvestmentDetails investmentDetails = investmentDetailsBuilder
                    .withFundName(fund.getName())
                    .withShare(fundsGroup.getSharePerFund() + remainder)
                        .build();

            remainder = 0;

            investmentResults.addInvestmentDetails(investmentDetails);
        }
    }

    private int getTotalInvestmentAmount(TotalInvestmentAmountProvider totalInvestmentAmountProvider) {
        return totalInvestmentAmountProvider.getTotalInvestmentAmount();
    }

    public void setSharePerFundCalculationService(SharePerFundCalculationService sharePerFundCalculationService) {
        this.sharePerFundCalculationService = sharePerFundCalculationService;
    }

    public void setFundService(FundService fundService) {
        this.fundService = fundService;
    }

    public void setFundsGroupExtractorService(FundsGroupExtractorService fundsGroupExtractorService) {
        this.fundsGroupExtractorService = fundsGroupExtractorService;
    }

    public void setPercentageCalculatorService(PercentageCalculatorService percentageCalculatorService) {
        this.percentageCalculatorService = percentageCalculatorService;
    }
}
package pl.ksz.investfunds.api;

public interface RateProvider {
    int getRate();
}

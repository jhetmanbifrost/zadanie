package pl.ksz.investfunds.api.service;

import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;

public interface PercentageCalculatorService {
    String getComputedPercentage(int share);
    void setTotalInvestmentAmountProvider(TotalInvestmentAmountProvider investFundsController);
}

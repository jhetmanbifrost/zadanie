package pl.ksz.investfunds.api.service;

import pl.ksz.investfunds.model.RatesConfiguration;
import pl.ksz.investfunds.model.RatesConfigurationId;

public interface RatesConfigurationService {
    RatesConfiguration getByConfigurationId(RatesConfigurationId ratesConfigurationId);
}

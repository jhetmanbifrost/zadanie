package pl.ksz.investfunds.api.service;

import pl.ksz.investfunds.model.Fund;

import java.util.List;

public interface FundService {
    List<Fund> findAll();
    List<Fund> findByIdIsIn(List<Long> ids);
}

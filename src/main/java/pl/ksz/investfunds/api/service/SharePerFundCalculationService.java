package pl.ksz.investfunds.api.service;

import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;
import pl.ksz.investfunds.model.FundType;
import pl.ksz.investfunds.model.InvestingStyle;

public interface SharePerFundCalculationService {
    int getSharePerFundsGroup(TotalInvestmentAmountProvider totalInvestmentAmountProvider,
                              InvestingStyle investingStyle,
                              FundType fundType);
}

package pl.ksz.investfunds.api.service;

import pl.ksz.investfunds.api.RateProvider;
import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;

public interface InvestingFundShareResolverService {
    int getShare(RateProvider rateProvider, TotalInvestmentAmountProvider totalInvestmentAmountProvider);
}

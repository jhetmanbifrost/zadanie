package pl.ksz.investfunds.api.service;

import pl.ksz.investfunds.model.Fund;
import pl.ksz.investfunds.model.FundType;
import pl.ksz.investfunds.results.FundsGroup;

import java.util.List;

public interface FundsGroupExtractorService {
    FundsGroup extract(List<Fund> funds, FundType fundType);
}

package pl.ksz.investfunds.api;

public interface TotalInvestmentAmountProvider {
    int getTotalInvestmentAmount();
}

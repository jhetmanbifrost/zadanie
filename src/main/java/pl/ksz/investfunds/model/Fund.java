package pl.ksz.investfunds.model;

public class Fund extends HasId<Long> {

    private String name;
    private FundType type;

    public Fund(final Long id, final String name, final FundType type) {
        super(id);
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public FundType getType() {
        return type;
    }
}

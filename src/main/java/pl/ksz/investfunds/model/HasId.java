package pl.ksz.investfunds.model;

public abstract class HasId<KEY_TYPE> {

    private KEY_TYPE id;

    public HasId(KEY_TYPE id) {
        this.id = id;
    }

    public KEY_TYPE getId() {
        return id;
    }

}

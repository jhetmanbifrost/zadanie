package pl.ksz.investfunds.model;

public enum FundType {

    POLISH("Polski"),
    FOREIGN("Zagraniczny"),
    MONEY("Pieniężny");

    private String name;

    FundType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

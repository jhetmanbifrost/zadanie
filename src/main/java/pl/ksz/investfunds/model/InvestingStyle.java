package pl.ksz.investfunds.model;

public enum InvestingStyle {

    SAFE("Bezpieczny"),

    BALANCED("Zrownowazony"),

    AGGRESSIVE("Agresywny");

    private String name;

    InvestingStyle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

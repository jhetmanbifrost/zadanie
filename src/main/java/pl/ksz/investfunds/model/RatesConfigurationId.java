package pl.ksz.investfunds.model;

import java.io.Serializable;
import java.util.Objects;

public class RatesConfigurationId implements Serializable {

    private FundType fundType;
    private InvestingStyle investingStyle;

    public RatesConfigurationId() {
    }

    public RatesConfigurationId(FundType fundType, InvestingStyle investingStyle) {
        this.fundType = fundType;
        this.investingStyle = investingStyle;
    }

    public FundType getFundType() {
        return fundType;
    }

    public InvestingStyle getInvestingStyle() {
        return investingStyle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RatesConfigurationId that = (RatesConfigurationId) o;
        return fundType == that.fundType &&
                investingStyle == that.investingStyle;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fundType, investingStyle);
    }
}

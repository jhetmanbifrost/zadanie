package pl.ksz.investfunds.model;

import pl.ksz.investfunds.api.RateProvider;

import java.util.Objects;

public class RatesConfiguration implements RateProvider {

    private RatesConfigurationId configurationId;
    private int rate;

    public RatesConfiguration(RatesConfigurationId ratesConfigurationId, int rate) {
        this.configurationId = ratesConfigurationId;
        this.rate = rate;
    }

    @Override
    public int getRate() {
        return rate;
    }

    public RatesConfigurationId getConfigurationId() {
        return configurationId;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setConfigurationId(RatesConfigurationId configurationId) {
        this.configurationId = configurationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RatesConfiguration)) return false;
        RatesConfiguration that = (RatesConfiguration) o;
        return rate == that.rate &&
                Objects.equals(configurationId, that.configurationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configurationId, rate);
    }
}

package pl.ksz.investfunds.service;

import pl.ksz.investfunds.api.service.RatesConfigurationService;
import pl.ksz.investfunds.model.FundType;
import pl.ksz.investfunds.model.InvestingStyle;
import pl.ksz.investfunds.model.RatesConfiguration;
import pl.ksz.investfunds.model.RatesConfigurationId;

import java.util.HashMap;
import java.util.Map;

public class RatesConfigurationServiceFakeImpl implements RatesConfigurationService {

    public static final RatesConfigurationId SAFE_POLISH = new RatesConfigurationId(FundType.POLISH, InvestingStyle.SAFE);
    public static final RatesConfigurationId SAFE_FOREIGN = new RatesConfigurationId(FundType.FOREIGN, InvestingStyle.SAFE);
    public static final RatesConfigurationId SAFE_MONEY = new RatesConfigurationId(FundType.MONEY, InvestingStyle.SAFE);

    public static final RatesConfigurationId BALANCED_POLISH = new RatesConfigurationId(FundType.POLISH, InvestingStyle.BALANCED);
    public static final RatesConfigurationId BALANCED_FOREIGN = new RatesConfigurationId(FundType.FOREIGN, InvestingStyle.BALANCED);
    public static final RatesConfigurationId BALANCED_MONEY = new RatesConfigurationId(FundType.MONEY, InvestingStyle.BALANCED);

    public static final RatesConfigurationId AGGRESSIVE_POLISH = new RatesConfigurationId(FundType.POLISH, InvestingStyle.AGGRESSIVE);
    public static final RatesConfigurationId AGGRESSIVE_FOREIGN = new RatesConfigurationId(FundType.FOREIGN, InvestingStyle.AGGRESSIVE);
    public static final RatesConfigurationId AGGRESSIVE_MONEY = new RatesConfigurationId(FundType.MONEY, InvestingStyle.AGGRESSIVE);

    private static final Map<RatesConfigurationId, RatesConfiguration> rateConfiguration =
            new HashMap<>();

    static {
        addRateConfiguration(SAFE_POLISH, 20);
        addRateConfiguration(SAFE_FOREIGN, 75);
        addRateConfiguration(SAFE_MONEY, 5);

        addRateConfiguration(BALANCED_POLISH, 30);
        addRateConfiguration(BALANCED_FOREIGN, 60);
        addRateConfiguration(BALANCED_MONEY, 10);

        addRateConfiguration(AGGRESSIVE_POLISH, 40);
        addRateConfiguration(AGGRESSIVE_FOREIGN, 20);
        addRateConfiguration(AGGRESSIVE_MONEY, 40);
    }

    @Override
    public RatesConfiguration getByConfigurationId(RatesConfigurationId ratesConfigurationId) {
        return rateConfiguration.get(ratesConfigurationId);
    }

    private static void addRateConfiguration(final RatesConfigurationId ratesConfigurationId, final int rate) {
        rateConfiguration.put(ratesConfigurationId, new RatesConfiguration(ratesConfigurationId, rate));
    }
}

package pl.ksz.investfunds.service;

import pl.ksz.investfunds.api.service.FundService;
import pl.ksz.investfunds.model.Fund;
import pl.ksz.investfunds.model.FundType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FundServiceFakeImpl implements FundService {

    private static final Map<Long, Fund> FUNDS = new HashMap<>();

    static {
        addFund(1L,"Fundusz polski 1", FundType.POLISH);
        addFund(2L,"Fundusz polski 2", FundType.POLISH);
        addFund(3L,"Fundusz polski 3", FundType.POLISH);

        addFund(4L, "Fundusz zagraniczny 1", FundType.FOREIGN);
        addFund(5L, "Fundusz zagraniczny 2", FundType.FOREIGN);
        addFund(6L, "Fundusz zagraniczny 3", FundType.FOREIGN);

        addFund(7L, "Fundusz pieniężny 1", FundType.MONEY);
        addFund(8L, "Fundusz pieniężny 2", FundType.MONEY);
    }

    public List<Fund> findByIdIsIn(final List<Long> ids){
        return findAll()
                .stream()
                .filter(fund -> ids.contains(fund.getId()))
                .collect(Collectors.toList());
    }

    public List<Fund> findAll() {
        return FUNDS.values().stream().collect(Collectors.toList());
    }

    private static void addFund(Long id, String fundName, FundType fundType) {
        FUNDS.put(id, new Fund(id, fundName, fundType));
    }
}

package pl.ksz.investfunds.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class RatesConfigurationTest {

    private RatesConfigurationId polishSafeRatesConfigurationId = new RatesConfigurationId(FundType.POLISH, InvestingStyle.SAFE);

    private RatesConfiguration actualRatesConfiguration = new RatesConfiguration(polishSafeRatesConfigurationId, 20);

    @Test
    public void testEquality() {
        //given:
        RatesConfiguration expectedRatesConfiguration = new RatesConfiguration(polishSafeRatesConfigurationId, 20);
        assertEqualsRatesConfigurations(expectedRatesConfiguration);
    }

    @Test
    public void testNotSameConfigIds() {
        RatesConfiguration expectedRatesConfiguration = new RatesConfiguration(null, 20);

        assertNotEqualsRatesConfigurations(expectedRatesConfiguration);
        assertNotEqualRatesConfigIds(expectedRatesConfiguration);
    }

    @Test
    public void testNotSameRates() {
        RatesConfiguration expectedRatesConfiguration = new RatesConfiguration(polishSafeRatesConfigurationId, 50);

        assertNotEqualsRatesConfigurations(expectedRatesConfiguration);
        assertEqualRatesConfigIds(expectedRatesConfiguration);

        assertNotEquals(expectedRatesConfiguration.getRate(), actualRatesConfiguration.getRate());
    }

    private void assertEqualsRatesConfigurations(RatesConfiguration expectedRatesConfiguration) {
        assertEquals(expectedRatesConfiguration, actualRatesConfiguration);
        assertNotSame(expectedRatesConfiguration, actualRatesConfiguration);


        assertEqualRatesConfigIds(expectedRatesConfiguration);

        assertEquals(expectedRatesConfiguration.getRate(), actualRatesConfiguration.getRate());
    }

    private void assertNotEqualsRatesConfigurations(RatesConfiguration expectedRatesConfiguration) {
        assertNotEquals(expectedRatesConfiguration, actualRatesConfiguration);
        assertNotSame(expectedRatesConfiguration, actualRatesConfiguration);
        assertNotEquals(expectedRatesConfiguration.hashCode(), actualRatesConfiguration.hashCode());
    }

    private void assertEqualRatesConfigIds(RatesConfiguration expectedRatesConfiguration) {
        assertEquals(expectedRatesConfiguration.getConfigurationId(), actualRatesConfiguration.getConfigurationId());
        assertSame(expectedRatesConfiguration.getConfigurationId(), actualRatesConfiguration.getConfigurationId());
        assertEquals(expectedRatesConfiguration.getConfigurationId().hashCode(),
                actualRatesConfiguration.getConfigurationId().hashCode());
    }

    private void assertNotEqualRatesConfigIds(RatesConfiguration expectedRatesConfiguration) {
        assertNotEquals(expectedRatesConfiguration.getConfigurationId(), actualRatesConfiguration.getConfigurationId());
        assertNotSame(expectedRatesConfiguration.getConfigurationId(), actualRatesConfiguration.getConfigurationId());
        assertNotEquals(expectedRatesConfiguration.hashCode(),
                actualRatesConfiguration.hashCode());
    }

}
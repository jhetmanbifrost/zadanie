package pl.ksz.investfunds.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class RatesConfigurationIdTest {

    // given:
    private RatesConfigurationId polishSafeRatesConfigurationId = new RatesConfigurationId(FundType.POLISH, InvestingStyle.SAFE);

    @Test
    public void testDefaultConstructor() {
        // given:
        RatesConfigurationId actualRatesConfigurationId = new RatesConfigurationId(FundType.POLISH, InvestingStyle.SAFE);
        RatesConfigurationId defaultRatesConfigurationId = new RatesConfigurationId();

        assertNotSame(defaultRatesConfigurationId, actualRatesConfigurationId);
        assertNotEquals(defaultRatesConfigurationId, actualRatesConfigurationId);
        assertNotEquals(defaultRatesConfigurationId.hashCode(), actualRatesConfigurationId.hashCode());
    }

    @Test
    public void testEqualityWhenOneRateConfigIdHasNullFundType() {
        // given:
        RatesConfigurationId actualRatesConfigurationId = new RatesConfigurationId(null, InvestingStyle.SAFE);

        assertDifferent(actualRatesConfigurationId);
    }

    @Test
    public void testEqualityWhenOneRateConfigIdHasNullInvestingStyle() {
        // given:
        RatesConfigurationId actualRatesConfigurationId = new RatesConfigurationId(FundType.POLISH, null);

        // then:
        assertDifferent(actualRatesConfigurationId);
    }

    @Test
    public void testEqualityWhenOneRateConfigIdHasDifferentInvestingStyle() {
        // given:
        RatesConfigurationId actualRatesConfigurationId = new RatesConfigurationId(FundType.POLISH, InvestingStyle.AGGRESSIVE);

        // then:
        assertDifferent(actualRatesConfigurationId);
    }

    @Test
    public void testEqualityWhenOneRateConfigIdHasDifferentFundType() {
        // given:
        RatesConfigurationId actualRatesConfigurationId = new RatesConfigurationId(FundType.FOREIGN, InvestingStyle.SAFE);

        // then:
        assertDifferent(actualRatesConfigurationId);
    }

    @Test
    public void testGetters() {
        RatesConfigurationId actualRatesConfigurationId = polishSafeRatesConfigurationId;
        assertSame(polishSafeRatesConfigurationId.getFundType(), actualRatesConfigurationId.getFundType());
        assertSame(polishSafeRatesConfigurationId.getInvestingStyle(), actualRatesConfigurationId.getInvestingStyle());
    }

    private void assertNotSameTestObjectsEqualsAndSameHashCodes(RatesConfigurationId actualRatesConfigurationId) {
            assertEquals(polishSafeRatesConfigurationId, actualRatesConfigurationId);
            assertNotSame(polishSafeRatesConfigurationId, actualRatesConfigurationId);
            assertEquals(polishSafeRatesConfigurationId.hashCode(), actualRatesConfigurationId.hashCode());
    }

    private void assertDifferent(RatesConfigurationId actualRatesConfigurationId) {
        assertNotEquals(polishSafeRatesConfigurationId, actualRatesConfigurationId);
        assertNotSame(polishSafeRatesConfigurationId, actualRatesConfigurationId);
        assertNotEquals(polishSafeRatesConfigurationId.hashCode(), actualRatesConfigurationId.hashCode());
    }
}
package pl.ksz.investfunds;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import pl.ksz.investfunds.api.service.*;
import pl.ksz.investfunds.model.InvestingStyle;
import pl.ksz.investfunds.results.InvestmentDetails;
import pl.ksz.investfunds.results.InvestmentResults;
import pl.ksz.investfunds.service.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InvestFundsServiceTest {

    private static final long POLISH_FUND_ID_1 = 1L;
    private static final long POLISH_FUND_ID_2 = 2L;
    private static final long POLISH_FUND_ID_3 = 3L;

    private static final long FOREIGN_FUND_ID_1 = 4L;
    private static final long FOREIGN_FUND_ID_2 = 5L;
    private static final long FOREIGN_FUND_ID_3 = 6L;


    private static final long MONEY_FUND_ID_1 = 7L;
    private static final long MONEY_FUND_ID_2 = 8L;
    private static final long MONEY_FUND_ID_3 = 9L;

    @InjectMocks
    private InvestFundsService investFundsController = new InvestFundsService();

    private FundService fundService = new FundServiceFakeImpl();
    private FundsGroupExtractorService fundsGroupExtractorService = new FundsGroupExtractorServiceImpl();

    private InvestingFundShareResolverService investingFundShareResolverService = new InvestingFundShareResolverServiceImpl();
    private RatesConfigurationService ratesConfigurationService = new RatesConfigurationServiceFakeImpl();
    private SharePerFundCalculationService sharePerFundCalculationService =
            new SharePerFundCalculationServiceImpl(ratesConfigurationService, investingFundShareResolverService);
    private PercentageCalculatorService percentageCalculatorService = new PercentageCalculatorServiceImpl();

    @Before
    public void setUp() {
        investFundsController.setFundService(fundService);
        investFundsController.setFundsGroupExtractorService(fundsGroupExtractorService);
        investFundsController.setSharePerFundCalculationService(sharePerFundCalculationService);
        investFundsController.setPercentageCalculatorService(percentageCalculatorService);
    }

    @Test
    public void testBasicExampleFromStage1() {
        // given:
        List<Long> fundsForStage1 = Arrays.asList(1L,2L,4L,5L,6L,7L);

        // when:
        InvestmentResults investmentResults = investFundsController.getComputedShares(10000, InvestingStyle.SAFE, fundsForStage1);

        // then:
        // then:
        assertEquals(0, investmentResults.getNotInvestedAmount());
        assertStage1And2Results(investmentResults);

    }

    @Test
    public void testUnusedFundsFromStage2() {
        // given:
        List<Long> fundsForStage1 = Arrays.asList(1L,2L,4L,5L,6L,7L);

        // when:
        final int totalInvestmentValue = 10001;
        InvestmentResults investmentResults = investFundsController.getComputedShares(totalInvestmentValue, InvestingStyle.SAFE, fundsForStage1);

        // then:
        assertEquals(1, investmentResults.getNotInvestedAmount());
        assertStage1And2Results(investmentResults);
    }


    @Test
    public void testPercentageRoundingAndFundGroupSharingFromStage3() {
        // given:
        List<Long> fundsForStage1 = Arrays.asList(1L,2L,3L,4L,5L,7L);

        // when:
        final int totalInvestmentValue = 10000;
        InvestmentResults investmentResults = investFundsController.getComputedShares(totalInvestmentValue, InvestingStyle.SAFE, fundsForStage1);

        // then:
        List<InvestmentDetails> investmentDetails = investmentResults.getInvestmentDetails();
        assertEquals(6, investmentDetails.size());

        assertFundsShareEquals(investmentDetails.get(0), "Fundusz polski 1", 668, "6.68%");
        assertFundsShareEquals(investmentDetails.get(1), "Fundusz polski 2", 666, "6.66%");
        assertFundsShareEquals(investmentDetails.get(2), "Fundusz polski 3", 666, "6.66%");
        assertFundsShareEquals(investmentDetails.get(3), "Fundusz zagraniczny 1", 3750, "37.5%");
        assertFundsShareEquals(investmentDetails.get(4), "Fundusz zagraniczny 2", 3750, "37.5%");
        assertFundsShareEquals(investmentDetails.get(5), "Fundusz pieniężny 1", 500, "5%");

    }

    @Test
    public void testAggressiveInvestingStyle() {
        // given:
        List<Long> fundsForStage1 = Arrays.asList(1L,2L,3L,4L,5L,7L);
        final int totalInvestmentValue = 123456;

        // when:
        InvestmentResults investmentResults = investFundsController.getComputedShares(totalInvestmentValue, InvestingStyle.AGGRESSIVE, fundsForStage1);

        // then:
        final int notInvestedAmount = investmentResults.getNotInvestedAmount();
        assertEquals(1, notInvestedAmount);

        List<InvestmentDetails> investmentDetails = investmentResults.getInvestmentDetails();
        assertEquals(6, investmentDetails.size());

        assertFundsShareEquals(investmentDetails.get(0), "Fundusz polski 1", 16462, "13.33%");
        assertFundsShareEquals(investmentDetails.get(1), "Fundusz polski 2", 16460, "13.33%");
        assertFundsShareEquals(investmentDetails.get(2), "Fundusz polski 3", 16460, "13.33%");
        assertFundsShareEquals(investmentDetails.get(3), "Fundusz zagraniczny 1", 12346, "10%");
        assertFundsShareEquals(investmentDetails.get(4), "Fundusz zagraniczny 2", 12345, "10%");
        assertFundsShareEquals(investmentDetails.get(5), "Fundusz pieniężny 1", 49382, "40%");

        int sum = investmentDetails.stream().mapToInt(investmentDetail -> investmentDetail.getShare()).sum();

        assertEquals(totalInvestmentValue, sum + notInvestedAmount);
    }

    @Test
    public void testBalancedInvestingStyle() {
        // given:
        List<Long> fundsForStage1 = Arrays.asList(1L,2L,3L,4L,5L,7L);
        final int totalInvestmentValue = 123456;

        // when:
        InvestmentResults investmentResults = investFundsController.getComputedShares(totalInvestmentValue, InvestingStyle.BALANCED, fundsForStage1);

        // then:
        final int notInvestedAmount = investmentResults.getNotInvestedAmount();
        assertEquals(2, notInvestedAmount);

        List<InvestmentDetails> investmentDetails = investmentResults.getInvestmentDetails();
        assertEquals(6, investmentDetails.size());

        assertFundsShareEquals(investmentDetails.get(0), "Fundusz polski 1", 12346, "10%");
        assertFundsShareEquals(investmentDetails.get(1), "Fundusz polski 2", 12345, "10%");
        assertFundsShareEquals(investmentDetails.get(2), "Fundusz polski 3", 12345, "10%");
        assertFundsShareEquals(investmentDetails.get(3), "Fundusz zagraniczny 1", 37037, "30%");
        assertFundsShareEquals(investmentDetails.get(4), "Fundusz zagraniczny 2", 37036, "30%");
        assertFundsShareEquals(investmentDetails.get(5), "Fundusz pieniężny 1", 12345, "10%");

        int sum = investmentDetails.stream().mapToInt(investmentDetail -> investmentDetail.getShare()).sum();

        assertEquals(totalInvestmentValue, sum + notInvestedAmount);
    }

    @Test
    public void testRemainderWhenOneTypeOfFundIsMissing() {
        // given:
        List<Long> onlypPolishAndForeignTypesSelected = Arrays.asList(1L,2L,3L,4L,5L,6L);

        // when:
        InvestmentResults investmentResults = investFundsController.getComputedShares(10001, InvestingStyle.SAFE, onlypPolishAndForeignTypesSelected);

        // then:
        assertEquals(501, investmentResults.getNotInvestedAmount());
    }

    @Test
    public void testRemainderIfFundsSelectionIsEmpty() {
        // given:
        List<Long> onlypPolishAndForeignTypesSelected = Collections.emptyList();

        // when:
        InvestmentResults investmentResults = investFundsController.getComputedShares(10001, InvestingStyle.SAFE, onlypPolishAndForeignTypesSelected);

        // then:
        assertEquals(10001, investmentResults.getNotInvestedAmount());
    }

    private void assertStage1And2Results(InvestmentResults investmentResults) {
        List<InvestmentDetails> investmentDetails = investmentResults.getInvestmentDetails();
        assertEquals(6, investmentDetails.size());

        assertFundsShareEquals(investmentDetails.get(0), "Fundusz polski 1", 1000, "10%");
        assertFundsShareEquals(investmentDetails.get(1), "Fundusz polski 2", 1000, "10%");
        assertFundsShareEquals(investmentDetails.get(2), "Fundusz zagraniczny 1", 2500, "25%");
        assertFundsShareEquals(investmentDetails.get(3), "Fundusz zagraniczny 2", 2500, "25%");
        assertFundsShareEquals(investmentDetails.get(4), "Fundusz zagraniczny 3", 2500, "25%");
        assertFundsShareEquals(investmentDetails.get(5), "Fundusz pieniężny 1", 500, "5%");
    }

    private void assertFundsShareEquals(InvestmentDetails investmentDetails, String name, int share, String percent) {
        assertEquals(name, investmentDetails.getFundName());
        assertEquals(share, investmentDetails.getShare());
        assertEquals(percent, investmentDetails.getComputedPercentage());
    }
}
package pl.ksz.investfunds.results;

import org.junit.Test;
import pl.ksz.investfunds.model.Fund;
import pl.ksz.investfunds.model.FundType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class FundsGroupTest {

    private FundsGroup testee = new FundsGroup(FundType.POLISH);

    @Test
    public void testFundsGroupParamsGivenEmptyFundsList() {
        // given
        List<Fund> empty = Collections.emptyList();
        testee.setFunds(empty);
        testee.setInvestment(3000);

        // when
        int actualSharePerFund = testee.getSharePerFund();

        // then:

        assertEquals(testee.getInvestmentPerGroup(), actualSharePerFund);
    }

    @Test
    public void testFundsGroupParamsGivenNullFundsList() {
        // given
        testee.setInvestment(3000);

        // when
        int actualSharePerFund = testee.getSharePerFund();

        // then:
        assertEquals(testee.getInvestmentPerGroup(), actualSharePerFund);
    }

    @Test
    public void testGetRemainder() {
        // given
        testee.setInvestment(3020);
        testee.setFunds(generateFundsList(3));

        // when
        int actualSharePerFund = testee.getSharePerFund();
        int actualRemainder = testee.getRemainder();


        // then
        assertEquals(1006, actualSharePerFund);
        assertEquals(2, actualRemainder);
    }

    @Test
    public void testGetNoRemainder() {
        // given
        testee.setInvestment(4011);
        testee.setFunds(generateFundsList(3));

        // when
        int actualSharePerFund = testee.getSharePerFund();
        int actualRemainder = testee.getRemainder();


        // then
        assertEquals(1337, actualSharePerFund);
        assertEquals(0, actualRemainder);
    }

    private List<Fund> generateFundsList(int numberOfElements) {
        List<Fund> fundsList = new ArrayList<>();
        for (int i = 0; i < numberOfElements; i++) {
            fundsList.add(mock(Fund.class));
        }

        return fundsList;
    }
}
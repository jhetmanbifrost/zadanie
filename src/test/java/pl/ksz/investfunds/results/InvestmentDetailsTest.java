package pl.ksz.investfunds.results;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import pl.ksz.investfunds.api.TotalInvestmentAmountProvider;
import pl.ksz.investfunds.api.service.PercentageCalculatorService;
import pl.ksz.investfunds.service.PercentageCalculatorServiceImpl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InvestmentDetailsTest {

    @Test
    public void testComputedPercentages() {
        // given:
        Share.forTheGivenShareOf(1668).expectedComputedPercentageStringIs("16.68%");

        Share.forTheGivenShareOf(1334).expectedComputedPercentageStringIs("13.34%");

        Share.forTheGivenShareOf(101).expectedComputedPercentageStringIs("1.01%");

        Share.forTheGivenShareOf(2500).expectedComputedPercentageStringIs("25%");

        Share.forTheGivenShareOf(250).expectedComputedPercentageStringIs("2.5%");

        Share.forTheGivenShareOf(0).expectedComputedPercentageStringIs("0%");
    }

    @Test
    public void testComputedPercentagesForTheTotalValueOf1000() {
        // given:
        Share.forTheGivenShareOf(167)
                .andGivenTheTotalInvestmentValueOf(1000)
                .expectedComputedPercentageStringIs("16.7%");

        Share.forTheGivenShareOf(132)
                .andGivenTheTotalInvestmentValueOf(1000)
                .expectedComputedPercentageStringIs("13.2%");

        Share.forTheGivenShareOf(11).andGivenTheTotalInvestmentValueOf(1000)
                .expectedComputedPercentageStringIs("1.1%");

        Share.forTheGivenShareOf(250)
                .andGivenTheTotalInvestmentValueOf(1000)
                .expectedComputedPercentageStringIs("25%");

        Share.forTheGivenShareOf(25)
                .andGivenTheTotalInvestmentValueOf(1000)
                .expectedComputedPercentageStringIs("2.5%");

        Share.forTheGivenShareOf(0)
                .andGivenTheTotalInvestmentValueOf(1000)
                .expectedComputedPercentageStringIs("0%");
    }
}

class Share {
    private static final int DEFAULT_INVESTMENT_VALUE = 10000;
    private int share;

    private TotalInvestmentAmountProvider totalInvestmentAmountProvider = mock(TotalInvestmentAmountProvider.class);

    public static Share forTheGivenShareOf(int givenShare) {
        return new Share(givenShare);
    }

    public Share andGivenTheTotalInvestmentValueOf(int totalInvestmentValue) {
        when(totalInvestmentAmountProvider.getTotalInvestmentAmount()).thenReturn(totalInvestmentValue);
        return this;
    }

    public void expectedComputedPercentageStringIs(String expectedPercentageString) {
        PercentageCalculatorService percentageCalculatorService = new PercentageCalculatorServiceImpl();
        percentageCalculatorService.setTotalInvestmentAmountProvider(totalInvestmentAmountProvider);
        InvestmentDetails.InvestmentDetailsBuilder investmentDetailsBuilder = new InvestmentDetails.InvestmentDetailsBuilder(percentageCalculatorService);
        InvestmentDetails investmentDetails = investmentDetailsBuilder.withFundName("Test Fund1").withShare(share).build();
        assertEquals(expectedPercentageString, investmentDetails.getComputedPercentage());
        totalInvestmentAmountProvider = null;
    }

    private Share(int share) {
        when(totalInvestmentAmountProvider.getTotalInvestmentAmount()).thenReturn(DEFAULT_INVESTMENT_VALUE);
        this.share = share;
    }
}
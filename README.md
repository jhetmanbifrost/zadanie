# Aplikacja realizująca zadanie dzielenia środków pomiędzy fundusze inwestycyjne.

## InvestFundsService

Główny serwis aplikacji. Zawiera metodę:

```
public InvestmentResults getComputedShares(final int totalInvestmentValue,
                                               final InvestingStyle investingStyle,
                                               final List<Long> chosenFunds);
											   
```

Metoda przyjmuje 3 parametry: 

1. całkowitą kwotę inwestycji
2. style inwestowania
3. listę ID wybranych funduszy

Zwraca obiekt typu InvestmentResults. 

## InvestmentResults

InvestmentResults przechowuje:

* listę funduszy wraz z ich udziałami (procent i kwota)
* całkowitą kwotę inwestycji
* nieprzydzieloną kwotę

TBC...
